package com.example.proyecto.controllers;

import com.example.proyecto.entities.*;
import com.example.proyecto.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Controller
public class LikeController {

    private LikeService likeService;
    private UserService userService;

    private RestaurantService restaurantService;
    @Autowired
    public void setLikeService(LikeService likeService) {
        this.likeService = likeService;
    }
    @Autowired
    public void setRestaurantService(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }
    @Autowired
    public void setUserService(UserService userService) {
        this.userService= userService;
    }

    @RequestMapping(value = "/like", method = RequestMethod.POST)
    String save(Likes like) {
        Authentication auth=SecurityContextHolder.getContext().getAuthentication();
        User user=userService.findByUsername(auth.getName());
        like.setUser(user);
        if(like.getUser()==null)
        {
            System.out.println("Error");
        }
        else
        {
            System.out.println("User id :"+ like.getUser().getId());
        }
        likeService.saveLike(like);
        return "redirect:/restaurant/"+like.getRestaurant().getId();
    }

    @RequestMapping(value ="/restaurantlike/{id}/{idrestaurant}",method = RequestMethod.GET)
    public String like(@PathVariable Integer id, @PathVariable Integer idrestaurant, Model model) {

        Restaurant restaurant=restaurantService.getRestaurant(idrestaurant);
        Likes likes=likeService.getLike(id);
        likes.setLikes(likes.getLikes()+1);
        likeService.saveLike(likes);

        return "redirect:/restaurant/"+restaurant.getId();

    }


}

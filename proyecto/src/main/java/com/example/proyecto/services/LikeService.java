
package com.example.proyecto.services;

import com.example.proyecto.entities.Likes;

public interface LikeService {
    Iterable<Likes> listAllLikes();

    void saveLike(Likes likes);

    Likes getLike(Integer id);

    void deleteLike(Integer id);
}

package com.example.proyecto.repository;

import com.example.proyecto.entities.Category;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface CategoryRepository extends CrudRepository<Category,Integer>{
}

package com.example.proyecto.repository;

import com.example.proyecto.entities.LevelRestaurant;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface LevelRestaurantRepository extends CrudRepository<LevelRestaurant,Integer> {
}

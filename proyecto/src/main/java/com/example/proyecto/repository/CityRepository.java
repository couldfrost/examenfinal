package com.example.proyecto.repository;

import com.example.proyecto.entities.City;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;
@Transactional
public interface CityRepository extends CrudRepository<City,Integer> {
}
